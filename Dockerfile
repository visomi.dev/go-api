FROM golang AS build
WORKDIR /go/src/gitlab.com/visomi.dev/go-api
COPY . .
ENV CGO_ENABLED=0
RUN go build -o /go/bin/go-api ./server.go

FROM scratch
COPY --from=build /go/bin/go-api /go/bin/go-api
CMD ["/go/bin/go-api"]
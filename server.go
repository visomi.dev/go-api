package main

import (
	"fmt"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

// ProductBase data
type ProductBase struct {
	Name  *string `sql:"index" json:"name"`
	Code  *string `sql:"index" json:"code"`
	Price *uint   `json:"price"`
}

// Product have the product data for database
type Product struct {
	ID        uint       `gorm:"primary_key" json:"id"`
	CreatedAt time.Time  `json:"createdAt"`
	UpdatedAt time.Time  `json:"updatedAt"`
	DeletedAt *time.Time `sql:"index" json:"deletedAt"`
	*ProductBase
}

// BaseResponse return a message and data
type BaseResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}

// CreateProductResponse return a message and data for the product created
type CreateProductResponse struct {
	BaseResponse
	Data *Product `json:"data"`
}

// UpdateProductResponse return a message and data for the product created
type UpdateProductResponse struct {
	BaseResponse
	Data *Product `json:"data"`
}

// GetAllProductsResponse return all products in database
type GetAllProductsResponse struct {
	BaseResponse
	Data []*Product `json:"data"`
}

// GetOneProductsResponse return one product in database
type GetOneProductsResponse struct {
	BaseResponse
	Data *Product `json:"data"`
}

func main() {
	db, err := gorm.Open("postgres", "postgres://tcpgqyol:t59UI8naIHw-FuQhLCsRtkfuf_RvwALH@ruby.db.elephantsql.com:5432/tcpgqyol")

	if err != nil {
		panic("failed to connect database")
	}

	defer db.Close()

	db.AutoMigrate(&Product{})

	e := echo.New()

	e.POST("/products", func(c echo.Context) error {
		p := new(ProductBase)

		if err = c.Bind(p); err != nil {
			return err
		}

		product := &Product{
			ProductBase: p,
		}

		db.Create(&product)

		return c.JSON(http.StatusCreated, &CreateProductResponse{
			BaseResponse: BaseResponse{
				Success: true,
				Message: "Product created",
			},
			Data: product,
		})
	})

	e.GET("/products", func(c echo.Context) error {
		products := []*Product{}

		db.Find(&products)

		return c.JSON(http.StatusCreated, &GetAllProductsResponse{
			BaseResponse: BaseResponse{
				Success: true,
				Message: "Products found",
			},
			Data: products,
		})
	})

	e.GET("/products/:id", func(c echo.Context) error {
		id := c.Param("id")
		i, err := strconv.Atoi(id)

		if err != nil {
			return err
		}

		product := new(Product)

		db.First(&product, i)

		return c.JSON(http.StatusCreated, &GetOneProductsResponse{
			BaseResponse: BaseResponse{
				Success: true,
				Message: "Product found",
			},
			Data: product,
		})
	})

	e.PUT("/products/:id", func(c echo.Context) error {
		id := c.Param("id")
		i, err := strconv.Atoi(id)

		if err != nil {
			return err
		}

		product := new(Product)

		db.First(&product, i)

		p := new(ProductBase)

		if err = c.Bind(p); err != nil {
			return err
		}

		product.Code = p.Code
		product.Name = p.Name
		product.Price = p.Price

		db.Save(&product)

		return c.JSON(http.StatusCreated, &UpdateProductResponse{
			BaseResponse: BaseResponse{
				Success: true,
				Message: "Product updated",
			},
			Data: product,
		})
	})

	e.DELETE("/products/:id", func(c echo.Context) error {
		id := c.Param("id")
		i, err := strconv.Atoi(id)

		if err != nil {
			return err
		}

		product := new(Product)

		db.First(&product, i)

		db.Delete(&product)

		return c.JSON(http.StatusCreated, &BaseResponse{
			Success: true,
			Message: "Product deleted",
		})
	})

	e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "${method} ${uri} -> ${status}, time: ${latency_human}\n",
	}))

	p := os.Getenv("PORT")

	if p == "" {
		p = "1234"
	}

	e.Logger.Fatal(e.Start(fmt.Sprintf(":%s", p)))
}
